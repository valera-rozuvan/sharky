#ifndef __MOVE_GEN_H__
#define __MOVE_GEN_H__

#include "board.h"

void moveGen(BOARD *cBoard);
void removeIllegalMoves(BOARD *cBoard);

#endif // __MOVE_GEN_H__
