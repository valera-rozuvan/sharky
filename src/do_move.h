#ifndef __DO_MOVE_H__
#define __DO_MOVE_H__

#include "board.h"

void doMove(BOARD *cBoard, unsigned long long move);
void undoMove(BOARD *cBoard);

#endif // __DO_MOVE_H__
