#include <stdio.h>
#include <stdlib.h>

#include "defs.h"
#include "board.h"
#include "board_routines.h"
#include "bitboard.h"
#include "zobrist_hashing.h"



/****************************************************************************
 *
 * START OF: Helper arrays used in functions below, and elsewhere.
 *
 * Related to the board, board routines, and the various pieces of board state.
 *
 **/

/*
 * Define arrays to convert indexes between two virtual representations
 * of the chess board.
 *
 * Board120. Array with length of 120. It can be represented as follows.
 * 10 by 12 items. In the center is the chess board. We have buffer
 * zones at the top, left, right, and bottom. Top and bottom
 * buffer zones have 2 rows to accommodate for generating knight
 * moves. Left and right buffer zones don't have 2 columns because
 * they extend (overlap) to each other.
 *
 * .-----------------------------------------------------------.
 * |     |     |     |     |     |     |     |     |     |     |
 * | 000 | 001 | 002 | 003 | 004 | 005 | 006 | 007 | 008 | 009 |
 * |-----------------------------------------------------------|
 * |     |     |     |     |     |     |     |     |     |     |
 * | 010 | 011 | 012 | 013 | 014 | 015 | 016 | 017 | 018 | 019 |
 * |-----.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~.-----|
 * |     I*****|     |*****|     |*****|     |*****|     I     |
 * | 020 I*021*| 022 |*023*| 024 |*025*| 026 |*027*| 028 I 029 |
 * |-----I-----------------------------------------------I-----|
 * |     I     |*****|     |*****|     |*****|     |*****I     |
 * | 030 I 031 |*032*| 033 |*034*| 035 |*036*| 037 |*038*I 039 |
 * |-----I-----------------------------------------------I-----|
 * |     I*****|     |*****|     |*****|     |*****|     I     |
 * | 040 I*041*| 042 |*043*| 044 |*045*| 046 |*047*| 048 I 049 |
 * |-----I-----------------------------------------------I-----|
 * |     I     |*****|     |*****|     |*****|     |*****I     |
 * | 050 I 051 |*052*| 053 |*054*| 055 |*056*| 057 |*058*I 059 |
 * |-----I-----------------------------------------------I-----|
 * |     I*****|     |*****|     |*****|     |*****|     I     |
 * | 060 I*061*| 062 |*063*| 064 |*065*| 066 |*067*| 068 I 069 |
 * |-----I-----------------------------------------------I-----|
 * |     I     |*****|     |*****|     |*****|     |*****I     |
 * | 070 I 071 |*072*| 073 |*074*| 075 |*076*| 077 |*078*I 079 |
 * |-----I-----------------------------------------------I-----|
 * |     I*****|     |*****|     |*****|     |*****|     I     |
 * | 080 I*081*| 082 |*083*| 084 |*085*| 086 |*087*| 088 I 089 |
 * |-----I-----------------------------------------------I-----|
 * |     I     |*****|     |*****|     |*****|     |*****I     |
 * | 090 I 091 |*092*| 093 |*094*| 095 |*096*| 097 |*098*I 099 |
 * |-----^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^-----|
 * |     |     |     |     |     |     |     |     |     |     |
 * | 100 | 101 | 102 | 103 | 104 | 105 | 106 | 107 | 108 | 109 |
 * |-----------------------------------------------------------|
 * |     |     |     |     |     |     |     |     |     |     |
 * | 110 | 111 | 112 | 113 | 114 | 115 | 116 | 117 | 118 | 119 |
 * ^-----------------------------------------------------------^
 *
 * Board64. Actual chess board has 64 squares, and will be represented as
 * rotated. Below you can see the visualization of the array length 64.
 * 8 by 8 items.
 *
 *         FILE_A  FILE_B  FILE_C  FILE_D  FILE_E  FILE_F  FILE_G  FILE_H
 *        .---------------------------------------------------------------.
 *        |       |       |       |       |       |       |       |       |
 * RANK_1 |00 [A1]|01 [B1]|02 [C1]|03 [D1]|04 [E1]|05 [F1]|06 [G1]|07 [H1]|
 *        |       |       |       |       |       |       |       |       |
 *        |---------------------------------------------------------------|
 *        |       |       |       |       |       |       |       |       |
 * RANK_2 |08 [A2]|09 [B2]|10 [C2]|11 [D2]|12 [E2]|13 [F2]|14 [G2]|15 [H2]|
 *        |       |       |       |       |       |       |       |       |
 *        |---------------------------------------------------------------|
 *        |       |       |       |       |       |       |       |       |
 * RANK_3 |16 [A3]|17 [B3]|18 [C3]|19 [D3]|20 [E3]|21 [F3]|22 [G3]|23 [H3]|
 *        |       |       |       |       |       |       |       |       |
 *        |---------------------------------------------------------------|
 *        |       |       |       |       |       |       |       |       |
 * RANK_4 |24 [A4]|25 [B4]|26 [C4]|27 [D4]|28 [E4]|29 [F4]|30 [G4]|31 [H4]|
 *        |       |       |       |       |       |       |       |       |
 *        |---------------------------------------------------------------|
 *        |       |       |       |       |       |       |       |       |
 * RANK_5 |32 [A5]|33 [B5]|34 [C5]|35 [D5]|36 [E5]|37 [F5]|38 [G5]|39 [H5]|
 *        |       |       |       |       |       |       |       |       |
 *        |---------------------------------------------------------------|
 *        |       |       |       |       |       |       |       |       |
 * RANK_6 |40 [A6]|41 [B6]|42 [C6]|43 [D6]|44 [E6]|45 [F6]|46 [G6]|47 [H6]|
 *        |       |       |       |       |       |       |       |       |
 *        |---------------------------------------------------------------|
 *        |       |       |       |       |       |       |       |       |
 * RANK_7 |48 [A7]|49 [B7]|50 [C7]|51 [D7]|52 [E7]|53 [F7]|54 [G7]|55 [H7]|
 *        |       |       |       |       |       |       |       |       |
 *        |---------------------------------------------------------------|
 *        |       |       |       |       |       |       |       |       |
 * RANK_8 |56 [A8]|57 [B8]|58 [C8]|59 [D8]|60 [E8]|61 [F8]|62 [G8]|63 [H8]|
 *        |       |       |       |       |       |       |       |       |
 *        ^---------------------------------------------------------------^
 *
 * So, we need two routines to be able to quickly convert between coordinates
 * of one array (64 size board) to the other (120 size board). For this we will have
 * precomputed arrays with indexes of one array pointing to values of indexes
 * of the other array. And vice versa.
 *
 * For example:
 *
 *   board64to120[13] = 36
 *   board120to64[42] = 17
 *
 */

unsigned char board64to120[64] = {
  21, 22, 23, 24, 25, 26, 27, 28,
  31, 32, 33, 34, 35, 36, 37, 38,
  41, 42, 43, 44, 45, 46, 47, 48,
  51, 52, 53, 54, 55, 56, 57, 58,
  61, 62, 63, 64, 65, 66, 67, 68,
  71, 72, 73, 74, 75, 76, 77, 78,
  81, 82, 83, 84, 85, 86, 87, 88,
  91, 92, 93, 94, 95, 96, 97, 98
};

unsigned char board120to64[120] = {
  99, 99, 99, 99, 99, 99, 99, 99, 99, 99,
  99, 99, 99, 99, 99, 99, 99, 99, 99, 99,
  99,  0,  1,  2,  3,  4,  5,  6,  7, 99,
  99,  8,  9, 10, 11, 12, 13, 14, 15, 99,
  99, 16, 17, 18, 19, 20, 21, 22, 23, 99,
  99, 24, 25, 26, 27, 28, 29, 30, 31, 99,
  99, 32, 33, 34, 35, 36, 37, 38, 39, 99,
  99, 40, 41, 42, 43, 44, 45, 46, 47, 99,
  99, 48, 49, 50, 51, 52, 53, 54, 55, 99,
  99, 56, 57, 58, 59, 60, 61, 62, 63, 99,
  99, 99, 99, 99, 99, 99, 99, 99, 99, 99,
  99, 99, 99, 99, 99, 99, 99, 99, 99, 99
};

unsigned char board120toFile[120] = {
  99, 99,     99,     99,     99,     99,     99,     99,     99,     99,
  99, 99,     99,     99,     99,     99,     99,     99,     99,     99,
  99, FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, 99,
  99, FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, 99,
  99, FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, 99,
  99, FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, 99,
  99, FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, 99,
  99, FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, 99,
  99, FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, 99,
  99, FILE_A, FILE_B, FILE_C, FILE_D, FILE_E, FILE_F, FILE_G, FILE_H, 99,
  99, 99,     99,     99,     99,     99,     99,     99,     99,     99,
  99, 99,     99,     99,     99,     99,     99,     99,     99,     99
};

const char *boardPieceStr[13] = {
  ".",
  "P", "N", "B", "R", "Q", "K",
  "p", "n", "b", "r", "q", "k"
};

const char *boardPieceWithoutColorStr[13] = {
  ".",
  "p", "n", "b", "r", "q", "k",
  "p", "n", "b", "r", "q", "k"
};

const char *BIT_CASTLING_CHAR[4] = {
  "K", "Q", "k", "q"
};

const char *FILE_NAMES[8] = {
  "A", "B", "C", "D", "E", "F", "G", "H"
};

const char *SQUARE_NAMES[64] = {
  "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1",
  "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
  "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
  "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
  "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
  "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
  "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
  "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"
};

/*
 *
 * END OF: Helper arrays used in functions below, and elsewhere.
 *
 ****************************************************************************/



/*
 *
 * sideToMoveStr() returns a string, representing the side to move for the current board state.
 *
 * There should ever be only two possible returns from the function "white" or "black". However,
 * just in case, we check.
 *
 * If the `side` variable is set to an unexpected value, we exit the program with an error.
 * Basically, this should never happen, and code should never reach that place (end of function).
 *
 **/
const char* sideToMoveStr(BOARD *cBoard)
{
  if (cBoard->side == WHITE) return "white";
  if (cBoard->side == BLACK) return "black";

  // In theory - we should never reach this line of code ;)
  exit(EXIT_FAILURE);
  return "?";
}

/*
 *
 * castlingPermStr() returns a string containing a single castling permission character, based on the bit
 * passed to the function as `bitToTest`.
 *
 * If the bit is not set in the castling permissions variable, the function returns "-".
 *
 * Because we store both white and black castling permissions in a single variable, unique bits are set for
 * each side and for each available castling right. For white side - upper case letters are returned. For
 * black side - lower case letters are returned.
 *
 **/
const char* castlingPermStr(BOARD *cBoard, unsigned char bitToTest)
{
  if (CHECK_BIT(cBoard->castlingPerm, bitToTest)) return BIT_CASTLING_CHAR[bitToTest];

  return "-";
}

/*
 *
 * printBoard() prints to stdout the board (squares and pieces), along with board legend (ranks, files),
 * and important board state (en passant, castling, etc.). White pieces are printed with upper case
 * letters black pieces are printed with lower case letters.
 *
 **/
void printBoard(BOARD *cBoard) {
  char rankIter = 0;
  char fileIter = 0;
  unsigned char sq120 = 0;

  printf("\n");

  rankIter = RANK_8;
  do {
    fileIter = FILE_A;

    printf("%hhi ", rankIter + 1);

    do {
      sq120 = FileRankTo120SQ(fileIter, rankIter);

      if (cBoard->pieces[sq120] >= 13) {
        printf("?");
      } else {
        printf("%3s", boardPieceStr[cBoard->pieces[sq120]]);
      }

      fileIter += 1;
    } while (fileIter <= FILE_H);

    printf("\n");
    rankIter -= 1;
  } while (rankIter >= RANK_1);

  printf("\n  ");
  fileIter = FILE_A;
  do {
    printf("%3c", 'a' + fileIter);

    fileIter += 1;
  } while (fileIter <= FILE_H);
  printf("\n\n");

  printf("# half moves: %hu\n", cBoard->historyPly);
  printf("# fiftyMove: %hhu\n", cBoard->fiftyMove);
  if (cBoard->enPassantFile != NO_EN_PASSANT) {
    printf("En passant file: %s\n", FILE_NAMES[cBoard->enPassantFile]);
  }
  printf("Position hash: %llx\n", cBoard->positionKey);
  printf("Side to move: %s\n", sideToMoveStr(cBoard));
  if (cBoard->castlingPerm != 0) {
    printf(
      "Castling rights: %s%s%s%s\n",
      castlingPermStr(cBoard, WKCastling),
      castlingPermStr(cBoard, WQCastling),
      castlingPermStr(cBoard, BKCastling),
      castlingPermStr(cBoard, BQCastling)
    );
  }
}

/*
 *
 * chessMoveToStr() processes a chess move, passed as 1st param `move`, and writes human readable move string
 * to the function's 2nd parameter `fmtdMove`. Internally, chess moves are stored as `unsigned long long`
 * numbers. Each bit of an internal move represents something about it. For example promotion information.
 *
 * For UCI protocol, we need the ability to pass moves as text. That's what this function is for.
 *
 **/
void chessMoveToStr(unsigned long long move, char fmtdMove[MAX_MOVE_STR_LENGTH])
{
  unsigned char fromSq64 = board120to64[move & 0xFFULL];
  unsigned char toSq64 = board120to64[(move >> 8) & 0xFFULL];

  // Only used for showing promoted piece, if applicable.
  unsigned char promotedPiece = 0ULL;

  if ((CHECK_BIT(move, MOVE_BIT_K_CASTLE)) && (toSq64 == 6)) {
    snprintf(fmtdMove, 5, "%s", "e1g1");
  } else if ((CHECK_BIT(move, MOVE_BIT_K_CASTLE)) && (toSq64 == 62)) {
    snprintf(fmtdMove, 5, "%s", "e8g8");
  } else if ((CHECK_BIT(move, MOVE_BIT_Q_CASTLE)) && (toSq64 == 2)) {
    snprintf(fmtdMove, 5, "%s", "e1c1");
  } else if ((CHECK_BIT(move, MOVE_BIT_Q_CASTLE)) && (toSq64 == 58)) {
    snprintf(fmtdMove, 5, "%s", "e8c8");
  } else if (CHECK_BIT(move, MOVE_BIT_PROMOTION)) {
    promotedPiece = (move >> 16) & 0xFFULL;

    CLEAR_BIT(promotedPiece, 4);
    CLEAR_BIT(promotedPiece, 5);
    CLEAR_BIT(promotedPiece, 6);
    CLEAR_BIT(promotedPiece, 7);

    snprintf(fmtdMove, 6, "%s%s%s", SQUARE_NAMES[fromSq64], SQUARE_NAMES[toSq64], boardPieceWithoutColorStr[promotedPiece]);
  } else {
    snprintf(fmtdMove, 5, "%s%s", SQUARE_NAMES[fromSq64], SQUARE_NAMES[toSq64]);
  }
}

/*
 *
 * printBestMove() prints to stdout best move found for the current board state.
 *
 * If no move was found, or if search did not run, or if board state is just after initialization,
 * then internally `bestMove` is set to 0ULL. In this case we print the nullmove, which in UCI protocol is
 * represented by the "0000" string.
 *
 **/
void printBestMove(BOARD *cBoard)
{
  char fmtdMove[MAX_MOVE_STR_LENGTH] = "";

  if (cBoard->bestMove == 0ULL) {
    printf("bestmove 0000\n");
    return;
  }

  chessMoveToStr(cBoard->bestMove, fmtdMove);
  printf("bestmove %s\n", fmtdMove);
}


/*
 *
 * printMoves() prints to stdout legal moves available in the current position. Legal moves have the bit
 * `MOVE_BIT_ILLEGAL` set to zero.
 *
 * Along with the moves, function prints total number of legal moves available.
 *
 * If no moves are available, function prints "[0]: none".
 *
 **/
void printMoves(BOARD *cBoard)
{
  unsigned long long move = 0ULL;
  unsigned char legalMovesCount = 0;
  unsigned char idx = 0;
  char fmtdMove[MAX_MOVE_STR_LENGTH] = "";

  if (cBoard->movesAvailable == 0) {
    printf("Moves available [0]: none\n");

    return;
  }

  legalMovesCount = 0;
  idx = 0;
  do {
    if (CHECK_BIT(cBoard->moves[idx], MOVE_BIT_ILLEGAL)) {
      idx += 1;
      continue;
    }

    legalMovesCount += 1;
    idx += 1;
  } while ((idx < cBoard->movesAvailable) && (idx < MAX_POSSIBLE_MOVES_IN_POSITION));

  printf("Moves available [%hu]: ", legalMovesCount);

  if (legalMovesCount == 0) {
    printf("none\n");

    return;
  }

  legalMovesCount = 0;
  idx = 0;
  do {
    move = cBoard->moves[idx];

    if (CHECK_BIT(move, MOVE_BIT_ILLEGAL)) {
      idx += 1;
      continue;
    }

    chessMoveToStr(move, fmtdMove);
    printf("%s; ", fmtdMove);

    legalMovesCount += 1;
    idx += 1;
  } while ((idx < cBoard->movesAvailable) && (idx < MAX_POSSIBLE_MOVES_IN_POSITION));

  printf("\n");
}

/*
 *
 * setupEmptyPosition() sets up initial board state to contain no pieces, and
 * the side to move as WHITE.
 *
 * All other state (such as castling permissions) are also initialized as best as possible according to the rules
 * of chess. Obviously an empty position is an illegal chess position (according to the official laws of chess).
 * But, such a function is handy when testing basic engine functionality. Therefore this function exists :)
 *
 * We set up board state (squares, pieces) manually, and don't use FEN parser. This way we can actually test FEN parser
 * for the empty position, and compare it to the one set up with this manual function.
 *
 */
void setupEmptyPosition(BOARD *cBoard)
{
  unsigned char idx = 0;

  for (idx = 0; idx < 120; idx += 1) {
    cBoard->pieces[idx] = NO_SQ;
  }

  for (idx = 0; idx < 64; idx += 1) {
    cBoard->pieces[board64to120[idx]] = EMPTY;
  }

  cBoard->castlingPerm = 0ULL;
  cBoard->side = WHITE;
  cBoard->enPassantFile = NO_EN_PASSANT;
  cBoard->fiftyMove = 0;
  cBoard->historyPly = 0;

  // We don't initialize cBoard->moves array because it will be populated by
  // moveGen() function at each run of the function. Also, the
  // variable cBoard->movesAvailable will be set and updated there.

  // We don't initialize the cBoard->numPieces array because it's done by
  // moveGen() function at each run of the function.

  cBoard->positionKey = generateFullHash(cBoard);
  cBoard->bestMove = 0ULL;
}

/*
 *
 * setupInitialPosition() sets up initial board state to the start of the game (start position, as described by official
 * chess rules). All other state (such as castling permissions) are also initialized according to the rules of chess.
 *
 * We set up board state (squares, pieces) manually, and don't use FEN parser. This way we can actually test FEN parser
 * for the start position, and compare it to the one set up with this manual function.
 *
 */
void setupInitialPosition(BOARD *cBoard)
{
  unsigned char idx = 0;

  for (idx = 0; idx < 120; idx += 1) {
    cBoard->pieces[idx] = NO_SQ;
  }

  for (idx = 0; idx < 64; idx += 1) {
    cBoard->pieces[board64to120[idx]] = EMPTY;
  }

  cBoard->pieces[A1] = wR;
  cBoard->pieces[B1] = wN;
  cBoard->pieces[C1] = wB;
  cBoard->pieces[D1] = wQ;
  cBoard->pieces[E1] = wK;
  cBoard->pieces[F1] = wB;
  cBoard->pieces[G1] = wN;
  cBoard->pieces[H1] = wR;

  for (idx = A2; idx <= H2; idx += 1) {
    cBoard->pieces[idx] = wP;
  }

  for (idx = A7; idx <= H7; idx += 1) {
    cBoard->pieces[idx] = bP;
  }

  cBoard->pieces[A8] = bR;
  cBoard->pieces[B8] = bN;
  cBoard->pieces[C8] = bB;
  cBoard->pieces[D8] = bQ;
  cBoard->pieces[E8] = bK;
  cBoard->pieces[F8] = bB;
  cBoard->pieces[G8] = bN;
  cBoard->pieces[H8] = bR;

  cBoard->castlingPerm = 0ULL;

  SET_BIT(cBoard->castlingPerm, WKCastling);
  SET_BIT(cBoard->castlingPerm, WQCastling);
  SET_BIT(cBoard->castlingPerm, BKCastling);
  SET_BIT(cBoard->castlingPerm, BQCastling);

  cBoard->side = WHITE;

  cBoard->enPassantFile = NO_EN_PASSANT;

  cBoard->fiftyMove = 0;
  cBoard->historyPly = 0;

  // We don't initialize cBoard->moves array because it will be populated by
  // moveGen() function at each run of the function. Also, the
  // variable cBoard->movesAvailable will be set and updated there.

  // We don't initialize the cBoard->numPieces array because it's done by
  // moveGen() function at each run of the function.

  cBoard->positionKey = generateFullHash(cBoard);
  cBoard->bestMove = 0ULL;
}

/*
 *
 * checkDrawByRepetition() goes through board history (previous positions), and tries to find matching `positionKey`
 * values (hashes of positions). If there are 3 matches (i.e. in the course of the current game, a position
 * was repeated 3 times), then the current game is a draw.
 *
 * Return 1 - if the game is drawn by repetition.
 * Return 0 - if the game is not drawn, and can continue.
 *
 */
unsigned char checkDrawByRepetition(BOARD *cBoard)
{
  unsigned long long latestHash = 0ULL;
  unsigned char repeatHashCount = 0;
  unsigned short idx = 0;

  if (cBoard->historyPly < 6) {
    return 0;
  }

  latestHash = cBoard->positionKey;
  repeatHashCount = 1;
  idx = cBoard->historyPly;

  do {
    idx -= 1;

    if (cBoard->history[idx].positionKey == latestHash) {
      repeatHashCount += 1;
    }

    if (repeatHashCount == 3) {
      return 1;
    }
  } while (idx != 0);

  return 0;
}
