#ifndef __ZOBRIST_HASHING_H__
#define __ZOBRIST_HASHING_H__

#include "board.h"

unsigned long long generateFullHash(BOARD *cBoard);

#endif // __ZOBRIST_HASHING_H__
